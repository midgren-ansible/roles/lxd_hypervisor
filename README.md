# lxd_hypervisor role

Install LXD on a host and init it according to a preseed file. After
this role has been applied, it is possible to run LXD containers on
the host.

This role sets up LXD cluster nodes. A single host has to be selected
as the first node, which is controlled by the variable `lxd_cluster_other_node`:
* The first node has this variable not set (or empty)
* All other nodes have the inventory name of the first node as value

**Please note:** It *is* possible to setup an entire cluster in a single
Ansible run. The first node will be initialized first and is available
and ready by the time the others are added.

Make sure the variable `lxd_bind_ip` is set (preferably in *host_vars*).

The role assumes name resolution is in place, so make sure to add
hosts to DNS before running it.

## Requirements

- ansible >= 2.8
  (This is when the snap module was added to Ansible)
- molecule >= 2.19

## Role Variables

* `lxd_is_in_cluster`

    Whether the host is part of an LXD cluster or not.

    NOTE: Currently only `true` is supported here.

    Default: `true`

* `lxd_cluster_other_node`

    See description above!

* `lxd_cluster_password`

    The password used to join new nodes to the cluster and to connect to
    the cluster with https. This is preferably stored using Ansible vault.

    No default value - this variable must be set in a cluster setup.

* `lxd_zfs_filesystem`

    ZFS fileset where LXD should store containers, images, snapshots
    etc. This value is either a zpool name (meaning LXD stuff is saved
    directly under the zpool) or zpool/filesystem (where filesystem is
    created at deployment if needed or must otherwise be empty).

    Example:
    ```yaml
    lxd_zfs_filesystem: rpool/lxd
    ```

* `lxd_bind_ip`

    The IP address on the host that should be used when cluster nodes
    communicate with each other. This could be the IP address on LAN
    or if a specific cluster network is setup, the IP address on that
    network.

* `lxd_fan_underlay_subnet`

    The subnet to use as the underlay for the FAN network, i.e. the
    network bearing the traffic on the FAN. Given in CIDR format. This
    should probably be the network the `lxd_bind_ip` is part of.

    A alternate value of `auto` is accepted and results in the network
    holding the default route will be used.

    Default: auto

* `lxd_domain`

    The LXD daemon will listen to `lxd_bind_ip` but will register
    itself in the cluster using the FQDN "hostname.`lxd_domain`". This
    gives the opportunity to use a separate network for the LXD
    cluster and use a separate domain for that so that a DNS can be
    setup to provide corresponding IP address if looking up the names.

    Note that name resolution must work for `lxd_domain`; the role
    will not setup this. This can be solved by adding the domain to a
    DNS or by adding entries to all hosts' `/etc/hosts`.

    Default: The `network_domain` of the host.

* `lxd_populate_hosts`

    If there is no DNS available for `lxd_domain`, the role can
    optionally add all hosts to each others /etc/hosts.

    This is a map of entries to add to /etc/hosts, where each entry
    is the FQDN as key and IP address as value.

    Default: undefined

    Example:
        node1.lxd.home: 192.168.1.1
        node2.lxd.home: 192.168.1.2
        node3.lxd.home: 192.168.1.3

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: lxd
```
