import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_cluster_up(host):
    assert 'fully operational' in host.check_output("sudo lxc cluster show `hostname` | egrep 'message:'")


def test_cluster_members(host):
    # We issue a command to list all cluster members and expect to find three nodes:
    assert host.check_output("sudo lxc cluster list | grep lxd | wc | awk '{print $1}'") == '3'
